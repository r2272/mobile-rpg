namespace CodeBase.Services.Input
{
    public interface IInputService
    {
        UnityEngine.Vector2 Axis { get; }

        bool IsAttackButtonUp();
    }
}